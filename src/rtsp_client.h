#ifndef RTSP_CLIENT_H
#define RTSP_CLIENT_H

typedef enum _RtspTransport
{
    RTSP_TRANSPORT_TCP,
    RTSP_TRANSPORT_UDP,
    RTSP_TRANSPORT_BUTT
}RtspTransportMode;

struct RtspClient;

struct RtspClient* rtsp_client_create(int fd);
void rtsp_client_destroy(struct RtspClient* thiz);

int rtsp_client_get_fd(struct RtspClient* thiz);

int rtsp_client_set_cseq(struct RtspClient* thiz, unsigned int cseq);
int rtsp_client_get_cseq(struct RtspClient* thiz);

char* rtsp_client_recv_request_string(struct RtspClient* thiz);

int rtsp_client_dispatch(struct RtspClient* thiz);

int rtsp_client_set_filename(struct RtspClient* thiz, char* filename);
char* rtsp_client_get_filename(struct RtspClient* thiz);

int rtsp_client_set_hostname(struct RtspClient* thiz, char* hostname);
char* rtsp_client_get_hostname(struct RtspClient* thiz);

int rtsp_client_set_rtp_port(struct RtspClient* thiz, int rtp_port);
int rtsp_client_get_rtp_port(struct RtspClient* thiz);

int rtsp_client_set_rtcp_port(struct RtspClient* thiz, int rtcp_port);
int rtsp_client_get_rtcp_port(struct RtspClient* thiz);

int rtsp_client_set_rtp_ser_port(struct RtspClient* thiz, int rtp_ser_port);
int rtsp_client_get_rtp_ser_port(struct RtspClient* thiz);

int rtsp_client_set_rtcp_ser_port(struct RtspClient* thiz, int rtcp_ser_port);
int rtsp_client_get_rtcp_ser_port(struct RtspClient* thiz);

int rtsp_client_set_ssrc(struct RtspClient* thiz, unsigned int ssrc);
unsigned int rtsp_client_get_ssrc(struct RtspClient* thiz);

int rtsp_client_set_seq(struct RtspClient* thiz, unsigned short seq);
unsigned short rtsp_client_get_seq(struct RtspClient* thiz);

int rtsp_client_set_timestamp(struct RtspClient* thiz, unsigned long timestamp);
unsigned int rtsp_client_get_timestamp(struct RtspClient* thiz);

int rtsp_client_set_transport_mode(struct RtspClient*thiz, RtspTransportMode  transport_mode);
RtspTransportMode rtsp_client_get_transport_mode(struct RtspClient*thiz);

int rtsp_client_set_session_id(struct RtspClient* thiz, int session_id);
int rtsp_client_get_session_id(struct RtspClient* thiz);




#endif /*RTSP_CLIENT_H*/
